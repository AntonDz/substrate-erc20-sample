#![cfg_attr(not(feature = "std"), no_std)]

#[macro_use]
extern crate thiserror;

use ink_lang as ink;
use ink_storage::traits::{PackedLayout, SpreadLayout, StorageLayout};
use scale::{Decode, Encode};
use scale_info::TypeInfo;

#[derive(
    Default,
    Eq,
    PartialEq,
    Debug,
    TypeInfo,
    Encode,
    Decode,
    StorageLayout,
    SpreadLayout,
    PackedLayout,
)]
pub struct Wallet {
    balance: u128,
}

#[derive(Debug, Error, TypeInfo, Encode, Decode)]
pub enum Error {
    #[error("not authorized")]
    NotAuthorized,
    #[error("arithmetic overflow")]
    ArithmeticOverflow,
    #[error("insufficient funds")]
    InsufficientFunds,
}

#[ink::contract]
mod erc20 {
    use super::*;
    use ink_storage::{collections::HashMap, Lazy};

    #[ink(storage)]
    pub struct Erc20 {
        name: Lazy<String>,
        tag: Lazy<String>,
        supply: Lazy<u128>,
        decimals: Lazy<u8>,
        authority: Lazy<AccountId>,
        wallets: HashMap<AccountId, Wallet>,
    }

    impl Erc20 {
        #[ink(constructor)]
        pub fn new(name: String, tag: String, decimals: u8) -> Self {
            let authority = Self::env().caller();

            Self {
                name: Lazy::new(name),
                tag: Lazy::new(tag),
                supply: Lazy::new(0),
                decimals: Lazy::new(decimals),
                authority: Lazy::new(authority),
                wallets: HashMap::new(),
            }
        }

        fn wallet_mut(&mut self, account: AccountId) -> &mut Wallet {
            self.wallets.entry(account).or_insert_with(Default::default)
        }

        #[ink(message)]
        pub fn balance_of(&self, of: AccountId) -> u128 {
            self.wallets
                .get(&of)
                .map(|wallet| wallet.balance)
                .unwrap_or(0)
        }

        #[ink(message)]
        pub fn supply(&self) -> u128 {
            *self.supply
        }

        #[ink(message)]
        pub fn mint(&mut self, target: AccountId, amount: u128) -> Result<(), Error> {
            let authority = *self.authority;

            if authority != self.env().caller() {
                return Err(Error::NotAuthorized);
            }

            let mut wallet = self.wallet_mut(target);

            wallet.balance = wallet
                .balance
                .checked_add(amount)
                .ok_or(Error::ArithmeticOverflow)?;

            *self.supply = self
                .supply
                .checked_add(amount)
                .ok_or(Error::ArithmeticOverflow)?;

            Ok(())
        }

        #[ink(message)]
        pub fn burn(&mut self, target: AccountId, amount: u128) -> Result<(), Error> {
            let authority = *self.authority;

            if authority != self.env().caller() {
                return Err(Error::NotAuthorized);
            }

            let mut wallet = self.wallet_mut(target);

            wallet.balance = wallet
                .balance
                .checked_sub(amount)
                .ok_or(Error::ArithmeticOverflow)?;

            *self.supply = self
                .supply
                .checked_sub(amount)
                .ok_or(Error::ArithmeticOverflow)?;

            Ok(())
        }

        #[ink(message)]
        pub fn transfer(&mut self, destination: AccountId, amount: u128) -> Result<(), Error> {
            let mut source = self.wallet_mut(self.env().caller());

            if source.balance < amount {
                return Err(Error::InsufficientFunds);
            }

            source.balance = source
                .balance
                .checked_sub(amount)
                .ok_or(Error::ArithmeticOverflow)?;

            let mut destination = self.wallet_mut(destination);

            destination.balance = destination
                .balance
                .checked_add(amount)
                .ok_or(Error::ArithmeticOverflow)?;

            Ok(())
        }
    }

    #[cfg(test)]
    mod tests {
        use super::*;

        use ink_lang as ink;

        const DEFAULT_CALLEE_HASH: [u8; 32] = [0x07; 32];
        const DEFAULT_ENDOWMENT: Balance = 1_000_000;
        const DEFAULT_GAS_LIMIT: Balance = 1_000_000;

        fn default_accounts() -> ink_env::test::DefaultAccounts<ink_env::DefaultEnvironment> {
            ink_env::test::default_accounts::<ink_env::DefaultEnvironment>().unwrap()
        }

        fn set_next_caller(caller: AccountId) {
            ink_env::test::push_execution_context::<ink_env::DefaultEnvironment>(
                caller,
                AccountId::from(DEFAULT_CALLEE_HASH),
                DEFAULT_GAS_LIMIT,
                DEFAULT_ENDOWMENT,
                ink_env::test::CallData::new(ink_env::call::Selector::new([0x00; 4])),
            )
        }

        #[ink::test]
        fn happy_path() {
            let accounts = default_accounts();

            set_next_caller(accounts.alice);
            let mut contract = Erc20::new("hello".into(), "bye".into(), 6);

            assert_eq!(*contract.authority, accounts.alice);
            assert_eq!(*contract.decimals, 6);
            assert_eq!(*contract.supply, 0);

            contract
                .mint(accounts.bob, 1_000_000_000)
                .expect("should not fail");

            assert_eq!(contract.balance_of(accounts.bob), 1_000_000_000);

            set_next_caller(accounts.bob);

            contract
                .transfer(accounts.charlie, 5_000_000)
                .expect("should not fail");
            assert_eq!(contract.balance_of(accounts.charlie), 5_000_000);
            assert_eq!(contract.balance_of(accounts.bob), 1_000_000_000 - 5_000_000);

            set_next_caller(accounts.alice);

            contract
                .burn(accounts.bob, 1_000_000_000 - 5_000_000)
                .expect("should not fail");

            assert_eq!(contract.balance_of(accounts.bob), 0);
            assert_eq!(contract.supply(), 5_000_000);
        }
    }
}
